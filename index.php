<?php
/**
 * File:  index.php
 * Creation Date: 04/12/2017
 * description:
 *
 * @author: canals
 */

use Illuminate\Database\Capsule\Manager as DB;
use Slim\Slim;
use wishlist\afficheur\AfficheurMessage;
use wishlist\controleur\admin\ControleurIdentification;
use wishlist\controleur\admin\ControleurItemAdmin;
use wishlist\controleur\admin\ControleurListeAdmin;
use wishlist\models\Item;
use wishlist\models\Liste;
use wishlist\models\Message;
use wishlist\models\Partage;
use wishlist\models\User;
use wishlist\vue\VueConsultant;
use wishlist\vue\VueParticipant;

require_once __DIR__ . '/vendor/autoload.php';

define('PATH', parse_ini_file('src/conf/conf_path.ini')['path']);

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new Slim();

session_start();

$app->get('/edit/:token/nouvItem/', function (string $token) {
    echoHead("Nouvel Item");
    $liste = ControleurListeAdmin::access($token);

    $chemin = Slim::getInstance()->urlFor("ajout_item_process", ["token" => $token]);

    echo "<h1>Ajout d'un item à la liste: </h1>";
    $vue = new VueParticipant([$liste]);
    $vue->render(5);
    echo <<<FORM
<form id="fgen" method="post" action="$chemin" enctype="multipart/form-data">
<div class="select">
<label>Nom</label>
<input type='text' name='Nom' placeholder='Nom' required>
<br><label>Prix</label>
<input type='number' step='0.01' name='Prix' placeholder='Prix' min='0' required><br>
</div>
<div class="select">
<label>Description</label>
<textarea name='Descr' placeholder="Description de l'item" lang='fr' required></textarea>
</div>
<div class="select">
<label>Image</label>
<input type="file" name="Image" accept="image/*">
</div>
<input class='bouton' type="reset" value="Annuler">
<input class='bouton' type = 'submit' name="valider" value="Valider">
</form>
FORM;
})->name('ajout_item');

$app->post('/edit/:token/ajouterItem/process/', function (string $token) {
    echoHead("Nouvel Item");

    echo "Traitement en cours";
    $newItem = new Item();
    $bool = ControleurItemAdmin::saveItem($newItem, $token, 'save');
    if ($bool)
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_liste", ["token" => $token]));
    else
        echo "<div class='erreur'>ÉCHEC - Enregistrement annulé. Verifiez le format de l'image et réessayez.</div>";

})->name('ajout_item_process');


$app->get('/listes', function () {
    echoHead("Vos Listes");

    if (isset($_GET['Suppr']) && $_GET['Suppr'] == 'true')
        echo "<div class = 'erreur'>Suppression effectuée.</div>";


    if (isset($_SESSION['PHP_AUTH_USER']))
        $listes = Liste::accesUser($_SESSION['PHP_AUTH_USER']);

    echo "<h1>Vos Listes</h1>\n";

    if (isset($listes) && $listes->count() > 0) {
        echo "<p>Consultez vos listes déjà existentes :</p>";
        echo "<form id='f1' method='post' action='" . Slim::getInstance()->urlFor("edit") . "'>";
        echo "<select name='Token'>";

        foreach ($listes as $l) {
            echo "<option value='" . $l->token . "'>" . $l->titre . "</option>";
        }
        echo "</select>";
        echo "<input type = 'submit' class='bouton'  value='Accéder'></form>";

        echo "<p>Ou créez une nouvelle liste :</p>";
    } else
        if (isset($_SESSION['PHP_AUTH_USER']))
            echo "<p>Vous n'avez aucune liste. Créez une nouvelle liste dès maintenant:</p>";
        else {
            echo "<p>Vous n'êtes pas connecté mais vous pouvez créez une nouvelle liste dès maintenant:</p>";
        }

    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("creation_liste") . "'>NOUVELLE LISTE</a>";
    echo "<h1>Vous avez une clef de modification? </h1>";
    $url = Slim::getInstance()->urlFor("edit");
    echo <<<FORM
    <form id='f1' method='post' action="$url">
    <label>Indiquez la clef pour modifier une liste:</label>
    <input type='text' name='Token' placeholder='Clef' size='50' required>
    <input class='bouton' type = 'submit'  value='Valider'>
    </form>
FORM;

})->name('liste_listes_simple');


$app->post('/edit', function () {
    echo "<h1>Affichage & Édition de la liste</h1><div class='erreur'>Chargement en cours</div>";
    $_POST['Token'];
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_liste", ["token" => $_POST['Token']]));
})->name("edit");

$app->get('/', function () {
    echoHead('Accueil');
    $urlCons = Slim::getInstance()->urlFor("consult");
    echo "<div class='fond'><h1>MyWishList.app</h1>";
    echo "<p>MyWishList est une application en ligne pour créer, modifier, gérer et partager des listes de cadeaux.</p>";
    echo "<p>Demandez à vos proches de participer à vos listes et planifier tous vos évènements.</p>";
    echo "<p>.</p>";

    echo <<<FORM
    <form id='f1' method='get' action="$urlCons ">
    <label>Indiquez la clef pour accéder à une liste partagée:</label>
    <input type='text' name='Token' placeholder='Clef' size='50' required>
    <input class='bouton' type = 'submit'  value='Valider'>
    </form>
FORM;
    echo "</div>";

})->name('home');

$app->post('/acces/consult', function () {
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor("consult") . "/" . $_POST['Token']);
})->name('acces_consultation');


//Édition et affichage d'une liste
$app->get('/edit/:token', function (string $token) {
    echoHead("Édition liste");
    $liste = ControleurListeAdmin::access($token);
    if ($liste != null) {
        echo "<h1>Affichage & Édition de la liste</h1>";
        $vue = new VueParticipant([$liste]);
        $vue->render(2);
        echo "<a class='bouton' href = '" . Slim::getInstance()->urlFor("ajout_item", ["token" => $token]) . "' > NOUVEL ITEM</a>";
        echo "<a class='bouton' href = '" . Slim::getInstance()->urlFor("editer_liste", ["token" => $token]) . "' > ÉDITER</a>";
        echo "<a class='bouton' href = '" . Slim::getInstance()->urlFor("partager_liste", ["token" => $token]) . "' > PARTAGER</a>";
        echo "<a class='bouton' href = '" . Slim::getInstance()->urlFor("arreter_partager_liste", ["token" => $token]) . "' > ARRETER LE PARTAGE</a><br>";

        sectionMessage($liste->messages(), $liste->no, "edit", $token);

    } else
        echo "Erreur 404 : La liste n'existe pas";
})->name('edit_liste');


//Création d'un lien de partage
$app->get('/edit/:token/share', function (string $token) {
    $liste = ControleurListeAdmin::access($token);
    if ($liste != null) {
        echoHead("Édition liste");
        echo "<h1>Partager une liste</h1>\n";
        $key = Partage::share($token);
        echo "<p>Vous avez deux possibilité pour le partage de votre liste :</p>\n" .
            "<ul>\n" .
            "<p>Une clef de partage  -  Renseignez là à l'accueil pour accéder à la liste</p>\n" .
            "<p>Un lien de partage  -  Partagez directement ce lien pour accéder à la liste</p>\n" .
            "</ul>\n";


        echo "<form id = 'fs'>" .
            "<label>Clef de partage: </label>" .
            "<input type = 'text' size = '40' value = '$key' ><br>\n" .
            "<label>Lien de partage: </label>" .
            "<input type = 'text' size = '40' value = '" . PATH . Slim::getInstance()->urlFor("consult") . "/$key' ><br></form>\n";

        echo "<br><a class='bouton' href = '" . Slim::getInstance()->urlFor("arreter_partager_liste", ["token" => $token]) . "' >ANNULER</a><br>";
    } else
        echo "Erreur 404 : La liste n'existe pas";
})->name('partager_liste');


//Suppression d'un lien de partage
$app->get('/edit/:token/unshare', function (string $token) {

    $liste = ControleurListeAdmin::access($token);
    if ($liste != null) {
        echoHead("Edition liste");
        echo "<h1>Arréter le partage</h1>\n";
        $bool = Partage::unshare($token);
        if ($bool) {
            echo "<p>Les clefs et liens de partage ont bien étés supprimés.<br>Il n'est désormais plus possible aux autres utilisateurs d'accéder à votre liste.</p>";
        } else {
            echo "<p>Aucune clef et aucun lien de partage n'ont été distribués.<br>Vos listes ne sont pas accessibles aux autres utilisateurs</p>";
        }
        echo "<br><a class='bouton' href = '" . Slim::getInstance()->urlFor("partager_liste", ["token" => $token]) . "' > PARTAGER</a><br>";
    } else
        echo "Erreur 404 : La liste n'existe pas";
})->name('arreter_partager_liste');


//Modification d'une liste
$app->get('/edit/:token/editer', function (string $token) {
    $liste = ControleurListeAdmin::access($token);
    if ($liste != null) {
        echoHead("Édition liste");
        echo "<h1>Édition de la liste</h1>";
        $cheminProcess = Slim::getInstance()->urlFor("editer_liste_process", ["token" => $token]);
        $cheminEffacer = Slim::getInstance()->urlFor("supprimer_liste", ["token" => $token]);
        $titre = ControleurListeAdmin::findToken($token)->titre;
        $description = ControleurListeAdmin::findToken($token)->description;
        echo <<<FORM
<form id="fedit" method="post" action="$cheminProcess">
    <div class="select">
    <label>Titre</label>
    <input type="text" name="Titre" size="100" value = "$titre" required><br>
    </div>
    <div class="select">
    <label>Description</label>
    <textarea name='Description' cols='100' lang='fr' required>$description</textarea><br>
    </div>
    <input class="bouton" type=reset value=Annuler>
    <button class="bouton" type="submit">Valider</button>
    <a class='bouton' href='$cheminEffacer'>SUPPRIMER LA LISTE</a>
</form>
FORM;
    } else
        echo "Erreur 404 : La liste n'existe pas";
})->name('editer_liste');


$app->get('/edit/:token/editer/effacer', function (string $token) {
    echoHead('SUPPRESSION');
    echo "<p class='erreur'>ATTENTION : Vous êtes sur le point d'effacer la totalité de la liste et de ses objets</p>";
    echo "<p>Voulez-vous vraiment effacer la liste ?</p>";
    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("edit_liste", ["token" => $token]) . "'><b>RETOUR</b></a>";
    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("supprimer_liste_process", ["token" => $token]) . "'>EFFACER</a>";
})->name("supprimer_liste");


$app->get('/edit/:token/editer/effacer/process', function (string $token) {
    echoHead('SUPPRESSION');
    $liste = ControleurListeAdmin::access($token);
    if ($liste != null) {
        echo "<p class='erreur'>Suppression en cours</p>";
        ControleurListeAdmin::deleteAll($token);
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor('liste_listes_simple') . "?Suppr=true");
    } else
        echo "Erreur 404 : La liste n'existe pas";
})->name("supprimer_liste_process");


$app->post('/edit/:token/editer/process', function (string $token) {
    $liste = ControleurListeAdmin::access($token);
    if ($liste != null) {
        echoHead("Édition liste");
        echo "Traitement en cours";
        $upListe = ControleurListeAdmin::findToken($token);
        $upListe->titre = filter_var($_POST['Titre'], FILTER_SANITIZE_STRING);
        $upListe->description = filter_var($_POST['Description'], FILTER_SANITIZE_STRING);
        $upListe->update();
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_liste", ["token" => $token]));
    } else
        echo "Erreur 404 : La liste n'existe pas";
})->name('editer_liste_process');


//Visualisation d'un item de la liste
$app->get('/edit/:token/:id', function (string $token, int $id) {
    echoHead('Objet');
    $item = ControleurItemAdmin::findControle($id, ControleurListeAdmin::findId($token));
    if ($item != null) {
        echo "<h1>Affichage détaillé de votre objet</h1>";
        $vue = new VueParticipant([$item]);
        $vue->render(3);
        echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("editer_item_liste", ["token" => $token, "id" => $id]) . "'>ÉDITER</a>\n";
        echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("suppression_item", ["token" => $token, "id" => $id]) . "'>SUPPRIMER</a>\n";
    } else
        echo "Erreur 404 : L'objet n'existe pas dans cette liste";
})->name('edit_item');


//Suppression d'un item
$app->get('/edit/:token/:id/delete', function (string $token, int $id) {
    echoHead('Objet');
    $item = ControleurItemAdmin::findControle($id, ControleurListeAdmin::findId($token));
    if ($item != null) {
        echo "Suppression de l'objet en cours";
        $item = ControleurItemAdmin::find($id);
        $item->supprimer();
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_liste", ["token" => $token]));
    } else
        echo "Erreur 404 : L'objet n'existe pas dans cette liste";
})->name('suppression_item');


//Édition d'un item
$app->get('/edit/:token/:id/editer', function (string $token, int $id) {
    echoHead('Objet');
    $item = ControleurItemAdmin::findControle($id, ControleurListeAdmin::findId($token));
    if ($item != null) {
        echo "<h1>Édition de l'objet</h1>";
        $cheminValid = Slim::getInstance()->urlFor("edition_item_process", ["token" => $token, "id" => $id]);
        $cheminSupprImg = Slim::getInstance()->urlFor("suppression_img_item", ["token" => $token, "id" => $id]);
        $nom = $item->nom;
        $prix = $item->tarif;
        $description = $item->descr;
        echo <<<FORM
<form id="fgen" method="post" action="$cheminValid" enctype="multipart/form-data">
<div class="select">
<label>Nom</label>
<input type='text' name='Nom' placeholder='Nom' value="$nom" required>
<br><label>Prix</label>
<input type='number' step='0.01' name='Prix' placeholder='Prix' min='0' value="$prix" required><br>
</div>
<div class="select">
<label>Description</label>
<textarea name='Descr' placeholder="Description de l'item" lang='fr' required>$description</textarea>
</div>
<div class="select">
<label>Image</label>
<input type="file" name="Image" accept="image/*">
</div>
<a class='bouton' href="$cheminSupprImg"> Supprimer lˊimage</a>
<input class='bouton' type="reset" value="Annuler">
<input class='bouton' type = 'submit' name="valider" value="Valider">
</form>
FORM;
    } else
        echo "Erreur 404 : L'objet n'existe pas dans cette liste";
})->name('editer_item_liste');


//Suppression de l'image d'un item
$app->get('/edit/:token/:id/editer/process/deleteImg', function (string $token, int $id) {
    $item = ControleurItemAdmin::findControle($id, ControleurListeAdmin::findId($token));
    if ($item != null) {
        if ($item->img != 'default.jpg' && file_exists('web/img/items/' . $item->img))
            unlink('web/img/items/' . $item->img);
        $item->img = 'default.jpg';
        $item->update();
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_item", ["token" => $token, "id" => $id]));
    } else
        echo "Erreur 404 : L'objet n'existe pas dans cette liste";
})->name('suppression_img_item');


//Process d'édition d'un item
$app->post('/edit/:token/:id/editer/process', function (string $token, int $id) {
    echoHead('Objet');
    $item = ControleurItemAdmin::findControle($id, ControleurListeAdmin::findId($token));
    if ($item != null) {
        $bool = ControleurItemAdmin::saveItem($item, $token, 'update');
        if ($bool)
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_item", ["token" => $token, "id" => $id]));
        else {
            echo "<div class='erreur'>ÉCHEC - Enregistrement annulé. Verifiez le format de l'image et réessayez.</div>";
            echo "<a href='" . Slim::getInstance()->urlFor("editer_item_liste", ["token" => $token, "id" => $id]) . "' class='bouton'>RETOUR</a>";
        }
    } else
        echo "Erreur 404 : L'objet n'existe pas dans cette liste";
})->name('edition_item_process');


$app->get('/creerListe/', function () {
    echoHead("Nouvelle liste");
    echo "<h1>Création d'une nouvelle liste</h1>";
    $chemin = Slim::getInstance()->urlFor('creation_liste_process');
    echo <<<FORM
<form id="f1" method="post" action="$chemin">
    <div class="select">
    <label>Titre</label>
    <input type="text" name="Titre" placeholder="Titre" required>
    </div>
    <div class="select">
    <label>Description</label>
    <textarea name='Description' placeholder=Description cols='100' lang='fr' required></textarea>
    </div>
    <input class="bouton" type=reset value=Effacer>
    <button class="bouton" type="submit">Valider</button>
</form>
FORM;

})->name('creation_liste');


$app->post('/creerListe/process', function () {
    echoHead("Nouvelle liste");
    $newListe = new Liste();
    $newListe->titre = filter_var($_POST['Titre'], FILTER_SANITIZE_STRING);
    $newListe->description = filter_var($_POST['Description'], FILTER_SANITIZE_STRING);
    $newListe->token = bin2hex(random_bytes(5));
    if (isset($_SESSION['PHP_AUTH_USER']))
        $newListe->user = $_SESSION['PHP_AUTH_USER'];
    else
        $newListe->user = 'noAccount';
    $newListe->save();
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_liste", ["token" => ControleurListeAdmin::last()->token]));
})->name('creation_liste_process');


$app->get('/consult/:token/item/:id', function ($token, $id) {
    echoHead('Consultation item');
    $item = ControleurItemAdmin::find($id);
    if ($item != null) {
        echo "<h1>Affichage détaillé de l'oobjet</h1>";
        $vue = new VueConsultant([$item], $token);
        $vue->render(3);
    } else {
        echo "Erreur 404 : L'objet n'existe pas ou  n'appartient pas à la liste <br>";
        echo "<a href=" . Slim::getInstance()->urlFor("consult") . "?Token=$token>Retournez à votre liste de token: $token</a>";
    }

})->name('consultation_item_liste');


//Consultation d'une liste
$app->get('/consult', function () {
    echoHead('Consulter');
    if (isset($_GET['Token']) && Partage::exist($_GET['Token'])) {
        $token = $_GET['Token'];
        $liste = ControleurListeAdmin::findToken(Partage::recherche_liste($token));
        echo "<h1>Consultation de la liste</h1>";
        $vue = new VueConsultant([$liste], $token);
        $vue->render(2);
        echo "<p>Attention, il ne s'agit que d'une liste de consultation</p>";
        echo "<p>Utilisez la clef suivante pour partager la liste: <b>$token</b></p>";

        sectionMessage($liste->messages(), $liste->no, "consult", $token);

    } elseif (isset($_GET['Token']) && !Partage::exist($_GET['Token'])) {
        echo "<div class = 'erreur'>Cette clef n'existe pas. Réessayez.</div>";
    }
    $urlCons = Slim::getInstance()->urlFor("consult");
    echo "<div class='fond'><h1>Consulter une liste</h1>";
    echo "<p>Demandez à vos proches de partager une liste. Saisissez ensuite la clef de partage ici !</p>";
    echo <<<FORM
    <form id='f1' method='get' action="$urlCons ">
    <label>Indiquez la clef pour accéder à une liste partagée:</label>
    <input type='text' name='Token' placeholder='Clef' size='50' required>
    <input type = 'submit' name='valider' value='Valider' class='bouton'>
</form>
FORM;
    echo "</div>";

})->name('consult');


$app->get('/account', function () {
    echoHead("Compte");
    ControleurIdentification::check('compte');
    if (isset($_GET['err']))
        echo "<div class='erreur'>ERREUR - Liste non-ajoutée: clef erronée ou Liste déjà associée à un compte</div>";
    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor('unlog') . "'>ME DÉCONNECTER</a>";
    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor('effacer_compte') . "'>SUPPRIMER LE COMPTE</a>";
    echo "<h1>Bienvenue " . $_SESSION['PHP_AUTH_USER'] . ".</h1>";
    echo "<h1>Ajouter une liste à votre compte</h1>";
    $url = Slim::getInstance()->urlFor("account_addList");
    echo <<<FORM
    <form id='f1' method='post' action="$url">
    <label>Indiquez la clef pour modifier une liste:</label>
    <input type='text' name='Token' placeholder='Clef' size='50' required>
    <input class='bouton' type = 'submit'  value='Valider'>
    </form>
FORM;
})->name('compte');

$app->post('/account/addList', function () {
    if (isset($_POST['Token']))
        ControleurListeAdmin::addUser($_POST['Token'], $_SESSION['PHP_AUTH_USER']);
})->name("account_addList");

$app->get('/account/create', function () {
    echoHead("Nouveau Compte");
    echo "<h1>Créer un compte pour créer et éditer des listes</h1>";
    if (isset($_GET['err'])) {
        if ($_GET['err'] == 1)
            echo "<div class='erreur'>Adresse mail déjà utilisée</div>";

    }
    $path = Slim::getInstance()->urlFor("creer_compte_process");
    echo <<<FORM
<form id='f1' method='post' action="$path">
    <label>Adresse mail: </label>
    <input type='email' name='mail' placeholder='Mail'  required>
    <label>Pseudonyme: </label>
    <input type='text' name='pseudo' placeholder='Pseudonyme'  required>
    <label>Mot de passe: </label>
    <input id="passwd" name="passwd" type="password" pattern="^\S{8,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Votre mot de passe doit faire 8 caractères minimum' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" placeholder="Mot de passe" required>
    <input id="passwd_two" name="passwd_two" type="password" pattern="^\S{8,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Mots de passe différents' : '');" placeholder="Confirmez votre mot de passe" required>
    <input type = 'submit' name='valider' value='Valider' class='bouton'>
</form>
FORM;

})->name('creer_compte');


$app->post('/account/create/process', function () {
    echoHead("Nouveau Compte");
    if (User::getByMail($_POST['mail']) != null)
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor("creer_compte") . '?err=1');

    $usr = new User();
    $usr->mail = filter_var($_POST['mail'], FILTER_SANITIZE_EMAIL);
    $usr->username = filter_var($_POST['pseudo'], FILTER_SANITIZE_STRING);
    $sel = random_int(1, 99999);
    $usr->passwd = md5($_POST['passwd'] . $sel);
    $usr->grainSel = $sel;
    $usr->save();
    $_SESSION['PHP_AUTH_USER'] = $usr->mail;
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor("compte"));
})->name('creer_compte_process');


$app->get('/login', function () {
    echoHead('Connexion');
    if (isset($_GET['err']))
        echo "<div class='erreur'>Utilisateur ou mot de passe incorrect</div>";
    if (isset($_GET['ref']) && $_GET['ref'] != '')
        $urlVal = Slim::getInstance()->urlFor("login_process") . "?ref=" . $_GET['ref'];
    else
        $urlVal = Slim::getInstance()->urlFor("login_process") . "?ref=home";
    $urlNew = Slim::getInstance()->urlFor("creer_compte");
    echo <<<LOG
<div class='fond'><h1>Veuillez renseigner votre identifiant et votre mot de passe</h1>
<form id='f1' method='post' action="$urlVal">
    <label>Adresse mail: </label>
    <input type='email' name='PHP_AUTH_USER' placeholder='mail'  required>
    <label>Mot de passe: </label>
    <input type='password' name='PHP_AUTH_PW' placeholder='mot de passe'  required>
    <input type = 'submit' value='Valider' class='bouton'>
    <a href="$urlNew" class="bouton">CRÉER UN COMPTE</a>
    </form>
    </div>
LOG;

})->name('login');


$app->post('/login/process', function () {
    if (ControleurIdentification::auth() == true)
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor($_GET['ref']));
    else
        Slim::getInstance()->redirect(Slim::getInstance()->urlFor("login") . "?err=1&ref=" . $_GET['ref']);
})->name('login_process');


$app->get('/unlog', function () {
    echoHead('Déconnexion');
    echo "<div class = 'erreur'>Déconnexion en cours</div>";
    ControleurIdentification::unlog();
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor('compte'));
})->name('unlog');

//suppression d'un compte
$app->get('/account/effacer', function () {
    echoHead('SUPPRESSION');
    ControleurIdentification::check('compte');
    echo "<div class = 'erreur'>ATTENTION: Vous êtes sur le point d'effacer votre compte, vos listes et vos objets. Cette opération est irréversible. </div>";
    echo "<p>Voulez-vous vraiment effacer votre compte ?</p>";
    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("compte") . "'><b>RETOUR</b></a>";
    echo "<a class='bouton' href='" . Slim::getInstance()->urlFor("effacer_compte_process") . "'>EFFACER</a>";

})->name('effacer_compte');


$app->get('/account/effacer/process', function () {
    echoHead('SUPPRESSION');
    ControleurIdentification::check('compte');
    User::deleteAll($_SESSION['PHP_AUTH_USER']);
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor('unlog'));
})->name('effacer_compte_process');

$app->post('/:no/addMessage/:origine/:token', function ($no, $origine, $token) {
    echoHead("Ajout d'un message");
    $message = new Message();
    $message->id_list = $no;
    $message->username = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
    $message->text = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
    $message->save();

    switch ($origine) {
        case "consult":
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("consult" . "?Token=$token"));
            break;
        case "edit":
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("edit_liste", ["token" => $token]));
            break;
        default:
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("home"));
            break;
    }
})->name('ajout_message');


$app->run();

echo "</body>";
echo "</html>";

function echoHead(string $titre)
{
    echo <<<HEAD
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8"/>
<title>MyWishList - $titre</title>\n
HEAD;
    echo "<link rel='stylesheet' href='" . PATH . "/web/css/mywishlist.css'>\n";
    echo "<link rel='icon' href='" . PATH . "/web/img/icones/logo.png'>\n";
    echo "</head>\n";
    echo "<body>\n";

    echo "<nav><ul>";
    echo "<li><a class='nom' href='" . Slim::getInstance()->urlFor('home') . "'><img class ='icn' src='" . PATH . "/web/img/icones/logo.png' alt='logo'><div class='nom'>MyWishList</div></a></li>";
    echo "<li><a href='" . Slim::getInstance()->urlFor('home') . "'><img src='" . PATH . "/web/img/icones/home.png' alt='home'><div>Accueil</div></a></li>";
    echo "<li><a href='" . Slim::getInstance()->urlFor('consult') . "'><img src='" . PATH . "/web/img/icones/item.png' alt='item'><div>Consulter une liste</div></a></li>";
    echo "<li><a href='" . Slim::getInstance()->urlFor('liste_listes_simple') . "'><img src='" . PATH . "/web/img/icones/liste.png' alt='liste'><div>Éditer vos listes</div></a></li>";
    echo "<li><a href='" . Slim::getInstance()->urlFor('compte') . "'><img src='" . PATH . "/web/img/icones/compte.png' alt='compte'><div>Votre compte</div></a></li>";
    echo "</ul></nav>";

}

function sectionMessage($messages, $no, $origine, $token)
{
    echo "<div>\n";
    echo "<h1>Messages</h1>\n";
    $chemin = Slim::getInstance()->urlFor('ajout_message', ['no' => $no, 'origine' => $origine, 'token' => $token]);
    echo <<<LOG
<form id='f1' method='post' action=$chemin>
    <input type='text' name='nom' placeholder='Votre nom'  required>
    <textarea class='select' name='message' placeholder='Message'  required></textarea>
    <input type = 'submit' value='Envoyer un message' class='bouton'>
</form></div>
LOG;
    echo AfficheurMessage::rendre($messages->get());
}

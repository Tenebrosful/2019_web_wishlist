<?php


namespace wishlist\afficheur;


use Illuminate\Database\Eloquent\Collection;

class AfficheurMessage
{
    public static function rendre(Collection $messages): string
    {
        $res = "<div class='messages'>\n";

        foreach ($messages as $message) {
            $res .= "<div class='message'>\n";
            $res .= "<div id='username'>$message->username</div>\n";
            $res .= "<div id='message'>$message->text</div>\n";
            $res .= "</div>";
        }

        $res .= "</div>";
        return $res;
    }
}
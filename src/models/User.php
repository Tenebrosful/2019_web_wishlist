<?php


namespace wishlist\models;


use Illuminate\Database\Eloquent\Model;
use wishlist\controleur\admin\ControleurListeAdmin;

/**
 * @method static where(string $colone, string $comparateur, string $valeur)
 */
class User extends Model
{
    public $timestamps = false;
    protected $table = 'user';


    static function getByMail(string $mail)
    {
        return User::where("mail", "=", "$mail")->first();
    }

    static function deleteAll(string $mail)
    {
        $listes = Liste::accesUser($mail);
        foreach ($listes as $l) {
            ControleurListeAdmin::deleteAll($l->token);
        }
        $usr = User::getByMail($mail);
        $usr->delete();

    }
}
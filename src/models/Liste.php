<?php


namespace wishlist\models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $colone, string $comparateur, string $valeur)
 */
class Liste extends Model
{
    public $timestamps = false;
    protected $table = 'liste';
    protected $primaryKey = 'no';

    public static function accesUser($usr)
    {
        return Liste::where("user", "=", "$usr")->get();
    }

    public function items()
    {
        return $this->hasMany('\wishlist\models\Item', 'liste_id');
    }

    public function messages()
    {
        return $this->hasMany('\wishlist\models\Message', 'id_list');
    }


    public static function find(int $id)
    {
        return Liste::where("no", "=", "$id")->first();
    }


    public static function findToken($token)
    {
        return Liste::where("token", "=", "$token");
    }

}
<?php


namespace wishlist\models;

use Illuminate;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $colone, string $comparateur, string $valeur)
 */
class Item extends Model
{
    public $timestamps = false;
    protected $table = 'item';
    protected $primaryKey = 'id';

    public static function find(int $id)
    {
        return Item::where("id", "=", "$id")->first();
    }

    public function recherche_liste()
    {
        return $this->belongsTo('\wishlist\models\Liste', 'liste_id')->first();
    }

    public function supprimer()
    {
        if ($this->img != "default.jpg")
            unlink('web/img/items/' . $this->img);
        $this->delete();
    }
}

<?php


namespace wishlist\models;

use Illuminate;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $colone, string $comparateur, string $valeur)
 */
class Partage extends Model
{
    public $timestamps = false;
    protected $table = 'partage';

    protected $primaryKey = 'id';

    public static function recherche_liste($tokenConsultation)
    {
        $partage = Partage::where('TokenConsultation', '=', "$tokenConsultation")->first();

        if ($partage == null) {
            return null;
        } else {
            return $partage->TokenModification;
        }
    }

    /*
     * Indique le token associé au token de modification
     * Si aucun token n'est associé, création d'un nouveau token de consultation
     */
    public static function share($token): String
    {
        $key = Partage::where('TokenModification', '=', "$token")->first();
        if ($key != null) {
            $key->toJson();
            $key = $key['TokenConsultation'];
            return $key;
        } else {
            $partage = new Partage();
            $partage->TokenModification = $token;
            $key = bin2hex(random_bytes(5));
            $partage->TokenConsultation = $key;
            $partage->save();
            return $key;
        }
    }

    public static function unshare($token): bool
    {
        $partage = Partage::where('TokenModification', '=', "$token")->first();
        if ($partage != null) {
            $partage->delete();
            return true;
        }
        return false;
    }

    public static function exist($token)
    {
        $partage = Partage::where('TokenConsultation', '=', "$token")->first();
        return ($partage != null);
    }
}

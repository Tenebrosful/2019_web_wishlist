<?php


namespace wishlist\controleur\admin;


use Slim\Slim;
use wishlist\models\Liste;
use wishlist\models\Partage;
use wishlist\models\User;

class ControleurListeAdmin
{
    public static function all()
    {
        return Liste::all();
    }

    public static function last()
    {
        return Liste::all()->last();
    }

    public static function find(int $id)
    {
        return Liste::where("no", "=", "$id")->first();
    }

    public static function access(string $token)
    {
        return Liste::where("token", "=", "$token")->first();
    }

    public static function findToken(string $token)
    {
        return Liste::where("token", "=", "$token")->first();
    }

    public static function findId(string $token)
    {
        $l = Liste::where("token", "=", "$token")->first();
        $l->toJson();
        return $l['no'];
    }

    public static function deleteAll(string $token)
    {
        $list = self::findToken($token);
        ControleurItemAdmin::deleteViaIdListe($list->no);
        Partage::unshare($list->token);
        $list->delete();
    }

    public static function addUser(string $token, string $user)
    {
        $list = Liste::where("token", "=", "$token")->where("user", "=", "noAccount")->first();
        if ($list == null || $list->user != 'noAccount')
            Slim::getInstance()->redirect(PATH . '/account?err=1');
        $list->user = $user;
        $list->user_id = User::getByMail($user)->id;
        $list->update();
        Slim::getInstance()->redirect(PATH . '/account');
    }
}
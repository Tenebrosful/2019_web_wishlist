<?php


namespace wishlist\controleur\admin;


use Slim\Slim;
use wishlist\models\User;

class ControleurIdentification
{

    static function auth()
    {
        $usr = User::getByMail($_POST['PHP_AUTH_USER']);
        if ($usr == null) {
            echo "<div class='erreur'>Utilisateur non trouvé</div>";
            $_POST['PHP_AUTH_USER'] = null;
            $_POST['PHP_AUTH_PW'] = null;
            return false;
        } elseif ($usr->passwd == md5($_POST['PHP_AUTH_PW'] . $usr->grainSel)) {
            $_POST['PHP_AUTH_USER'] = null;
            $_POST['PHP_AUTH_PW'] = null;
            session_destroy();
            session_start();
            $_SESSION['PHP_AUTH_USER'] = $usr->mail;
            return true;
        } else {
            echo "<div class='erreur'>Mot de passe incorect</div>";
            $_POST['PHP_AUTH_USER'] = null;
            $_POST['PHP_AUTH_PW'] = null;
            return false;
        }
    }

    static function check(string $ref)
    {

        if ($ref == '') {
            $ref = 'home';
        }

        if (!isset($_SESSION['PHP_AUTH_USER']))
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("login") . "?ref=" . $ref);
    }

    static function unlog()
    {
        session_destroy();
    }

}
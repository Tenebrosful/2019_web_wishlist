<?php


namespace wishlist\controleur\admin;


use wishlist\models\Item;
use wishlist\models\Liste;

class ControleurItemAdmin
{
    public static function all()
    {
        return Item::all();
    }

    public static function last()
    {
        return Item::all()->last();
    }

    public static function find(int $id)
    {
        return Item::where("id", "=", "$id")->first();
    }

    public static function findControle(int $id, string $liste_id)
    {
        return Item::where("id", "=", "$id")->where("liste_id", "=", "$liste_id")->first();
    }

    public static function deleteViaIdListe(int $id)
    {
        $items = Item::where("liste_id", "=", "$id")->get();
        foreach ($items as $i) {
            $i->supprimer();
        }
    }

    static function saveItem($item, string $token, string $type): bool
    {
        if (isset($_FILES['Image']) AND $_FILES['Image']['error'] == 0) {
            $infosfichier = pathinfo($_FILES['Image']['name']);
            $extension_upload = $infosfichier['extension'];
            $extensions_autorisees = array('jpg', 'jpeg', 'png', 'JPG', 'JPEG');
            if (in_array($extension_upload, $extensions_autorisees)) {
                $fileName = rand() . filter_var($_FILES['Image']['name'], FILTER_SANITIZE_URL);
                move_uploaded_file($_FILES['Image']['tmp_name'], 'web/img/items/' . $fileName);
                if ($item->img != 'default.jpg' && file_exists('web/img/items/' . $item->img) && $type == 'update')
                    unlink('web/img/items/' . $item->img);
                $item->img = $fileName;
            } else {
                return false;
            }
        }

        $item->tarif = $_POST['Prix'];
        $item->nom = filter_var($_POST['Nom'], FILTER_SANITIZE_STRING);
        $item->descr = filter_var($_POST['Descr'], FILTER_SANITIZE_STRING);

        if ($type == 'update') {
            $item->update();
            return true;
        }

        if ($type == 'save') {
            $item->liste_id = Liste::where("token", "=", "$token")->first()->no;
            if ($item->img == '')
                $item->img = 'default.jpg';
            $item->save();
            return true;
        }
        return false;
    }
}
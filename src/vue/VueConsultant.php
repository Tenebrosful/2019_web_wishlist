<?php


namespace wishlist\vue;


use wishlist\models\Item;
use wishlist\models\Liste;

class VueConsultant
{
    private $liste = [];
    private $token;

    public function __construct(array $tab, $token)
    {
        $this->liste = $tab;
        $this->token = $token;
    }

    public function render(int $i)
    {
        switch ($i) {
            case 1 :
                echo "ERREUR";
                break;

            case 2 :
                echo $this->renderListeAvecItems();
                break;

            case 3 :
                echo $this->renderItemDetaille();
                break;

            case 4 :
                echo $this->renderItemApercu();
                break;

            default :
                echo "ERREUR - parametre invalide - type de rendu inconnu";
                break;
        }
    }

    private function renderListeAvecItems(): string
    {
        $s = "<div>\n<ul>\n";
        foreach ($this->liste as $l) {
            $s .= '<div class=\'titreListe\'>' . $l['titre'] . '</div><div class=\'descListe\'>' . $l['description'] . "</div>\n";
            $t = Liste::find($l['no']);
            $vueItems = new VueConsultant($t->items()->get()->toArray(), $this->token);
            $s .= $vueItems->renderItemApercu();
        }
        $s .= "</ul>\n</div>\n";
        return $s;
    }

    //Render de l'affichage d'une liste et de ses items

    private function renderItemApercu(): String
    {
        $url = $this->getUrl() . "consult/" . $this->token . "/item/";
        $s = "<section class='items'>";

        foreach ($this->liste as $i) {
            $s .= "<a href=$url" . $i['id'] . ">";
            $img = PATH . "/web/img/items/" . $i['img'];
            $s .= "<div class='item_aperçu' style='background-image: url($img)'>";
            $s .= "<span id='nom'>" . $i['nom'] . "</span>\n";
            $s .= "<span id='prix'>" . $i['tarif'] . " </span>\n";
            $s .= "</div></a>\n";

        }

        $s .= "</section>";
        return $s;
    }

    //Render pour l'affichage détaillé d'un item

    private function getUrl(): string
    {
        return PATH . '/';
    }

    // Render pour l'affichage des items
    private function renderItemDetaille(): string
    {
        $url = $this->getUrl() . "consult?Token=" . $this->token;
        $s = "<section>";

        foreach ($this->liste as $i) {
            $s .= "<div class='item_détail'>";
            $s .= "<div><img class='img-item' src=' " . PATH . "/web/img/items/" . $i['img'] . " ' alt='Item n°" . $i['id'] . " (" . $i['nom'] . ")'></div>\n";
            $s .= "<b>" . $i['nom'] . "</b>\n";
            $s .= "<p>" . $i['tarif'] . " </p>\n";
            $s .= "<cite>" . $i['descr'] . "</cite></div>\n";

            $itmp = Item::find($i['id']);
            $tmp = $itmp->recherche_liste();
            if ($tmp != null)
                $s .= "<td><a href=$url>" . $tmp['titre'] . "</a> </td>\n";
        }

        $s .= "</div>\n</section>";
        return $s;
    }



}
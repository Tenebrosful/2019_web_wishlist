<?php


namespace wishlist\vue;


use wishlist\models\Item;
use wishlist\models\Liste;

class VueParticipant
{
    private $liste = [];

    public function __construct(array $tab)
    {
        $this->liste = $tab;
    }

    public function render(int $i)
    {
        switch ($i) {
            case 1 :
                echo $this->renderListe();
                break;

            case 2 :
                echo $this->renderListeAvecItems();
                break;

            case 3 :
                echo $this->renderItemDetaille();
                break;

            case 4 :
                echo $this->renderItemApercu();
                break;

            case 5 :
                echo $this->renderListeGeneral();
                break;

            default :
                echo "parametre invalide - type de rendu inconnu";
                break;
        }
    }

    //Render de l'affichage d'un tableau de liste.s
    private function renderListe(): string
    {
        $url = $this->getUrl() . "liste/";

        $s = "<div>\n<table class='listes'>\n";
        $s .= "<tr class='top_table'>\n" .
            "<th>Liste</th>\n" .
            "<th>Expire le</th>\n" .
            "<th>Token</th>" .
            "</tr>\n";

        foreach ($this->liste as $l) {

            $s .= "<tr class='line_table'>\n";
            $s .= "<td><a href=$url" . $l['no'] . ">" . $l['titre'] . "</a> </td>\n" .

                "<td>" . $l['expiration'] . " </td>\n" .
                "<td>" . $l['token'] . "</td>" .
                "</tr>\n";

        }
        $s .= " </table>\n</div>\n";
        return $s;
    }

    private function getUrl(): string
    {
        return PATH . '/';
    }

    //Render de l'affichage d'une liste et de ses items
    private function renderListeAvecItems(): string
    {
        $s = "<div>\n";
        foreach ($this->liste as $l) {
            $s .= "<div class='titreListe'>" . $l['titre'] . "</div><div class='descListe'>" . $l['description'] . "</div>\n";
            if (!isset($_SESSION['PHP_AUTH_USER']))
                echo "<p>Utilisez la clef suivante pour retrouver votre liste: <b>" . $l['token'] . "</b></p>";
            $t = new Liste();
            $t->no = $l['no'];

            $vueItems = new VueParticipant($t->items()->get()->toArray());
            $s .= $vueItems->renderItemApercu();
        }
        $s .= "</div>\n";
        return $s;
    }

    //Render pour l'affichage détaillé d'un item
    private function renderItemDetaille(): string
    {
        $s = "<div>";

        foreach ($this->liste as $i) {
            $s .= "<div class='item_détail'>";
            $s .= "<img class='img-item' src=' " . PATH . "/web/img/items/" . $i['img'] . " ' alt='Item n°" . $i['id'] . " (" . $i['nom'] . ")'></div>\n";
            $s .= "<b>" . $i['nom'] . "</b>\n";
            $s .= "<p>" . $i['tarif'] . " € </p>\n";
            $s .= "<div class='descr'><b>Description</b>\n";
            $s .= "<p>" . $i['descr'] . "</p>\n";

            $itmp = Item::find($i['id']);
            $tmp = $itmp->recherche_liste();
            $s .= "<b>Retour à la liste </b>\n";
            $s .= "<a href=" . $this->getUrl() . "edit/" . $tmp['token'] . ">" . $tmp['titre'] . "</a></div>\n";

        }

        $s .= "</div>";
        return $s;
    }

    // Render pour l'affichage des items
    private function renderItemApercu(): String
    {
        $s = "<div class='items'>";

        foreach ($this->liste as $i) {
            $itmp = Item::find($i['id']);
            $tmp = $itmp->recherche_liste();
            $s .= "<a href=" . $tmp['token'] . "/" . $i['id'] . ">";
            $img = PATH . "/web/img/items/" . $i['img'];
            $s .= "<div class='item_aperçu' style='background-image: url($img)'>";
            $s .= "<span id='nom'>" . $i['nom'] . "</span>\n";
            $s .= "<span id='prix'>" . $i['tarif'] . " </span>\n";
            $s .= "</div></a>\n";

        }

        $s .= "</div>";
        return $s;
    }

    // Render de l'affichage des généralités d'une liste
    private function renderListeGeneral(): String
    {
        $s = "<div>\n";


        foreach ($this->liste as $l) {

            $s .= "<div class='titreListe'>" . $l['titre'] . "</div>\n" .

                "<div class='descListe'>" . $l['description'] . " </div>\n";

        }
        $s .= "</div>\n";
        return $s;
    }
}
